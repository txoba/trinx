var grass = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_grass, false, true);
if(grass != noone){
    instance_destroy();
    with(grass){
        instance_destroy();
    }
}
var enemy1 = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_eFollow, false, true);
if(enemy1 != noone){
    instance_destroy();
    with(enemy1){
        instance_destroy();
    }
}
var enemy2 = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_eNotFall, false, true);
if(enemy2 != noone){
    instance_destroy();
    with(enemy2){
        instance_destroy();
    }
}

if(place_meeting(x,y,obj_wall)){
 instance_destroy();
}
if(place_meeting(x,y,obj_sticky)){
 instance_destroy();
}
if(place_meeting(x,y,obj_killer)){
 instance_destroy();
}
if(place_meeting(x,y,obj_platform)){
 instance_destroy();
}
