bLeft = false;
bRight = false;
bTop = false;
bBot = false;

//block right
if(place_meeting(x+64,y,obj_block)){
    bRight = true;
}else{
    bRight = false;
}
//block left
if(place_meeting(x-64,y,obj_block)){
    bLeft = true;
}else{
    bLeft = false;
}
//block top
if(place_meeting(x,y-64,obj_block)){
    bTop = true;
}else{
    bTop = false;
}
//block bot
if(place_meeting(x,y+64,obj_block)){
    bBot = true;
}else{
    bBot = false;
}


    var rot = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_rotation, false, true);
    if(rot != noone){
        if(global.rotation == 0){
            global.rotation = 1;
        }else if(global.rotation == 1){
            global.rotation = 0;
        }
        with(rot){
            instance_destroy();
        }
    }
    
    if(global.rotation == 1){
    if (keyboard_check_pressed(ord("E"))){
        audio_play_sound(snd_e, 10, false);
        if(sprite_index == spr_player){
            image_index += 1;
        if (image_index == 4) image_index = 0;
        }else if(sprite_index == spr_blueRight && bBot==false){
            sprite_index = spr_blueDown;
        }else if(sprite_index == spr_blueDown && bLeft==false){
            sprite_index = spr_blueLeft;
        }else if(sprite_index == spr_blueLeft && bTop==false){
            sprite_index = spr_blueUp;
        }else if(sprite_index == spr_blueUp && bRight==false){
            sprite_index = spr_blueRight;
        }
    }
    if (keyboard_check_pressed(ord("Q"))){
        audio_play_sound(snd_q, 10, false);
        if(sprite_index == spr_player){
            image_index -= 1;
            if (image_index == 0) image_index = 4;
        }else if(sprite_index == spr_blueRight && bTop==false){
            sprite_index = spr_blueUp;
        }else if(sprite_index == spr_blueDown && bRight==false){
            sprite_index = spr_blueRight;
        }else if(sprite_index == spr_blueLeft && bBot==false){
            sprite_index = spr_blueDown;
        }else if(sprite_index == spr_blueUp && bLeft==false){
            sprite_index = spr_blueLeft;
        }
    }
    }

