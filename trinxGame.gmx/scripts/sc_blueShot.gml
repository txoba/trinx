//Horizontal Collision
if(place_meeting(x+hspeed,y,obj_wall)){
        while(!place_meeting(x+sign(hspeed),y,obj_wall)){
            x += sign(hspeed);
            }   
       hspeed= 0;
    instance_destroy();
}
if(place_meeting(x+hspeed,y,obj_grass)){
        while(!place_meeting(x+sign(hspeed),y,obj_grass)){
            x += sign(hspeed);
            }
            hspeed= 0;
            instance_destroy();
}
if(place_meeting(x+hspeed,y,obj_sticky)){
        while(!place_meeting(x+sign(hspeed),y,obj_sticky)){
            x += sign(hspeed);
            }
            hspeed= 0;
            instance_destroy();
}
if(place_meeting(x+hspeed,y,obj_killer)){
        while(!place_meeting(x+sign(hspeed),y,obj_killer)){
            x += sign(hspeed);
            }
          hspeed= 0;
          instance_destroy();
}

x += hspeed;

//Vertical Collision
if(place_meeting(x,y+vspeed,obj_wall)){
    while(!place_meeting(x,y+sign(vspeed),obj_wall)){
        y += sign(vspeed);
    }
    vspeed= 0;
    instance_destroy();
}
if(place_meeting(x,y+vspeed,obj_grass)){
    while(!place_meeting(x,y+sign(vspeed),obj_grass)){
        y += sign(vspeed);
    }
    vspeed= 0;
    instance_destroy();
}
if(place_meeting(x,y+vspeed,obj_sticky)){
    while(!place_meeting(x,y+sign(vspeed),obj_sticky)){
        y += sign(vspeed);
    }
    vspeed= 0;
    instance_destroy();
}
if(place_meeting(x,y+vspeed,obj_killer)){
    while(!place_meeting(x,y+sign(vspeed),obj_killer)){
        y += sign(vspeed);
    }
    vspeed= 0;
    instance_destroy();
}
y += vspeed;


