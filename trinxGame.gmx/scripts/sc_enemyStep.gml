move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;
//jumping enemy
if(jumpingman && place_meeting(x,y+8,obj_wall) && 
    !position_meeting(x+(sprite_width*2)*dir,y+(sprite_height/2)+8,obj_wall)){
    key_jump = true;
}else if(jumpingman){key_jump = false;}

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}

//Horizontal Collision
if(place_meeting(x+hsp,y,obj_wall)){
        while(!place_meeting(x+sign(hsp),y,obj_wall)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_grass)){
        while(!place_meeting(x+sign(hsp),y,obj_grass)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_sticky)){
        while(!place_meeting(x+sign(hsp),y,obj_sticky)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_killer)){
        while(!place_meeting(x+sign(hsp),y,obj_killer)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_platform)){
        while(!place_meeting(x+sign(hsp),y,obj_platform)){
            x += sign(hsp);
            }
       hsp= 0;
}
x+= hsp;

//Vertical Collision
if(place_meeting(x,y+vsp,obj_wall)){
    while(!place_meeting(x,y+sign(vsp),obj_wall)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_grass)){
    while(!place_meeting(x,y+sign(vsp),obj_grass)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_sticky)){
    while(!place_meeting(x,y+sign(vsp),obj_sticky)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_killer)){
    while(!place_meeting(x,y+sign(vsp),obj_killer)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_platform)){
    while(!place_meeting(x,y+sign(vsp),obj_platform)){
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;


//ground collision
if(fearofheights && place_meeting(x,y+8,obj_wall) && 
    !position_meeting(x+(sprite_width/2)*dir,y+(sprite_height/2)+8,obj_wall)){
    dir *= -1;
}
