
if (keyboard_check_pressed(ord("W"))){
    audio_play_sound(snd_w, 10, false);
    if(global.blueP==1){
        shot = instance_create(x,y,obj_shot);
    }
    if(global.blueP==2){
        shot = instance_create(x,y,obj_blueShot);
        global.grassP += 1;
        global.blueP = 1;
        
        if(sprite_index = spr_blueRight){
            sprite_index = spr_player;
            image_index = 0;
        }
        if(sprite_index = spr_blueLeft){
            sprite_index = spr_player;
            image_index = 2;
        }
        if(sprite_index = spr_blueDown){
            sprite_index = spr_player;
            image_index = 1;
        }
        if(sprite_index = spr_blueUp){
            sprite_index = spr_player;
            image_index = 3;
        }            
    }
}
