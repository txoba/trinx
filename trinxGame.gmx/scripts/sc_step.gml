//Get the player's input
key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_down = keyboard_check(vk_down);
key_up = -keyboard_check(vk_up);
key_jump = keyboard_check_pressed(vk_space);



//React to inputs
move = key_left + key_right;
hsp = move * movespeed;

if(place_meeting(x,y+1,obj_platform)){
    move2 = key_up + key_down;
    vsp = move2 * movespeed;
}

if(vsp < 18){
    vsp += grav;
}
if(place_meeting(x,y+1,obj_wall)){
    vsp = key_jump * -jumpspeed;
    grounded = true;
}
if(place_meeting(x,y+1,obj_grass)){
    vsp = key_jump * -jumpspeed;
    grounded = true;
}
if(place_meeting(x,y+1,obj_sticky)){
    vsp = key_jump * -jumpspeed;
    grounded = true;
}
if(place_meeting(x,y+1,obj_killer)){
    vsp = key_jump * -jumpspeed;
    grounded = true;
}
if(place_meeting(x,y+1,obj_platform)){
    move2 = key_up + key_down;
    vsp = move2 * movespeed;
}

if(global.blueP = 1){
if (image_index == 0){
    var blueRight = collision_rectangle(bbox_left, bbox_top, bbox_right+5, bbox_bottom, obj_sticky, false, true);
    if(blueRight != noone){
        sprite_index = spr_blueRight;
        global.blueP = 2;
        with(blueRight){
        instance_destroy();
        }
    }
}
}
if(global.blueP = 1){
if (image_index == 2){
    var blueLeft = collision_rectangle(bbox_left-5, bbox_top, bbox_right, bbox_bottom, obj_sticky, false, true);
    if(blueLeft != noone){
    sprite_index = spr_blueLeft;
    global.blueP = 2;
        with(blueLeft){
        instance_destroy();
        }
    }
}
}
if(global.blueP = 1){
if (image_index == 1){
    var blueDown = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom+5, obj_sticky, false, true);
    if(blueDown != noone){
    sprite_index = spr_blueDown;
    global.blueP = 2;
        with(blueDown){
        instance_destroy();
        }
    }
}
}
if(global.blueP = 1){
if (image_index == 3){
    var blueUp = collision_rectangle(bbox_left, bbox_top-5, bbox_right, bbox_bottom, obj_sticky, false, true);
    if(blueUp != noone){
    sprite_index = spr_blueUp;
    global.blueP = 2;
        with(blueUp){
        instance_destroy();
        }
    }
}
}

//Horizontal Collision
if(place_meeting(x+hsp,y,obj_wall)){
        while(!place_meeting(x+sign(hsp),y,obj_wall)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_grass)){
        while(!place_meeting(x+sign(hsp),y,obj_grass)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_sticky)){
        while(!place_meeting(x+sign(hsp),y,obj_sticky)){
            x += sign(hsp);
            }
       hsp= 0;
}
if(place_meeting(x+hsp,y,obj_killer)){
        while(!place_meeting(x+sign(hsp),y,obj_killer)){
            x += sign(hsp);
            }
       hsp= 0;
}

//killer 

var kill = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_redBlock, false, true);
    if(kill != noone){
        if(global.killer == 0){
            global.killer = 1;
        }else if(global.killer == 1){
            global.killer = 0;
        }
        with(kill){
            instance_destroy();
        }
    }

if(global.killer == 1){
    if(collision_rectangle(bbox_left, bbox_top-5, bbox_right, bbox_bottom, obj_killer, 0,0)){ //top
        if(image_index!=2){
            global.lifes -= 1;
        }
    }
    if(collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom+5, obj_killer, 0,0)){ //bottom
        if(image_index!=0){
            global.lifes -= 1;
        }
    }
    if(collision_rectangle(bbox_left, bbox_top, bbox_right+5, bbox_bottom, obj_killer, 0,0)){ //right
        if(image_index!=3){
            global.lifes -= 1;
        }
    }
    if(collision_rectangle(bbox_left-5, bbox_top, bbox_right, bbox_bottom, obj_killer, 0,0)){ //left
        if(image_index!=1){
            global.lifes -= 1;
        }
    }
}

if(global.killer == 0){
    if(place_meeting(x,y+1,obj_killer)){
        global.lifes -= 1;
    }
}

//killer end

//platform colider
if(collision_rectangle(bbox_left, bbox_top+5, bbox_right, bbox_bottom, obj_platform, 0,0)){
    while(!collision_rectangle(bbox_left, bbox_top+5, bbox_right, bbox_bottom, obj_platform, 0,0)){
            x -= sign(hsp);
          }
      hsp= 0;
}
x += hsp;
//platform colider end

//if (vsp < 0){
//Vertical Collision
if(place_meeting(x,y+vsp,obj_wall)){
    while(!place_meeting(x,y+sign(vsp),obj_wall)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_grass)){
    while(!place_meeting(x,y+sign(vsp),obj_grass)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_sticky)){
    while(!place_meeting(x,y+sign(vsp),obj_sticky)){
        y += sign(vsp);
    }
    vsp = 0;
}
if(place_meeting(x,y+vsp,obj_killer)){
    while(!place_meeting(x,y+sign(vsp),obj_killer)){
        y += sign(vsp);
    }
    vsp = 0;
}

if (image_index == 1 || sprite_index == spr_blueDown){
    if(place_meeting(x,y+vsp,obj_platform)){
        while(!place_meeting(x,y-sign(vsp),obj_platform)){
            y += sign(vsp);
        }
        vsp = 0;
    }
}else{
    if(place_meeting(x,y+vsp,obj_platform)){
        while(!place_meeting(x,y+sign(vsp),obj_platform)){
            y += sign(vsp);
        }
        vsp = 0;
    }
}
y += vsp;


